FROM debian:latest
LABEL maintainer="Ashish Shah <ashneo76@gmail.com>"
LABEL description="Nginx and PHP base image with supervisor."

RUN apt-get update; apt-get -yq install supervisor nginx bash make
ADD ./supervisord.conf /etc/supervisor/supervisord.conf
RUN mkdir -p /etc/supervisor/conf.d \
             /var/log/supervisor

ADD supervisord/nginx.conf /etc/supervisor/conf.d/nginx.conf
RUN mkdir -p /run/nginx; chown -R www-data:www-data /run/nginx

RUN apt-get -y -q install php7.0 php7.0-fpm \
                 php7.0-mcrypt php7.0-gmp php7.0-intl \
                 php7.0-gd php7.0-xml php7.0-json

EXPOSE 80 443
CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]
