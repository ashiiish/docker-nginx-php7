.PHONY: build

IMG_NAME = ashneo76/nginx-php7
IMG_VERSION = v1
IMG_REGISTRY = registry.gitlab.com

build:
	docker build -t $(IMG_REGISTRY)/$(IMG_NAME):$(IMG_VERSION) .

deploy:
	docker push $(IMG_REGISTRY)/$(IMG_NAME):$(IMG_VERSION)

run:
	docker run -p 8080:80 $(IMG_REGISTRY)/$(IMG_NAME):$(IMG_VERSION)
